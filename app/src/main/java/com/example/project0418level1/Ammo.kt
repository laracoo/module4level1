package com.example.project0418level1

import kotlin.random.Random

enum class Ammo(val damage: Int, val chanceOfCriticalDamage: Int, val criticalDamageRation: Int) {
    PLASTIC(5, 6, 2),
    IRON(15, 4, 2),
    GOLD(20, 3, 2),
    TITANIUM(30, 2, 2);

    fun totalDamage(): Int {
        return if (Random.nextInt() < chanceOfCriticalDamage) {
            this.damage * criticalDamageRation
        } else {
            this.damage
        }
    }
}
