package com.example.project0418level1

open class JuniorWarrior : AbstractWarrior(60, 3, 6, Weapons.gun) {
    override var isKilled: Boolean = false
}
