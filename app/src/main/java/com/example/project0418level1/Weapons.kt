package com.example.project0418level1

object Weapons {
    val gun = object : AbstractWeapon(10, FireType.SingleShot) {
        override fun createPatron(): Ammo {
            return Ammo.PLASTIC
        }
    }

    val machineGun: AbstractWeapon = object : AbstractWeapon(15, FireType.MultiShot) {
        override fun createPatron(): Ammo {
            return Ammo.IRON
        }
    }

    val revolver: AbstractWeapon = object : AbstractWeapon(20, FireType.SingleShot) {
        override fun createPatron(): Ammo {
            return Ammo.GOLD
        }
    }

    val rifle: AbstractWeapon = object : AbstractWeapon(30, FireType.MultiShot) {
        override fun createPatron(): Ammo {
            return Ammo.TITANIUM
        }
    }
}
