package com.example.project0418level1

sealed class FireType(val countOfShots: Int) {
    object SingleShot : FireType(1)
    object MultiShot : FireType(3)
}
