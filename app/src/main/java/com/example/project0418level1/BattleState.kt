package com.example.project0418level1

sealed class BattleState {
    data class Progress(val commandAHealth: Int, val commandBHealth: Int) : BattleState()
    object TeamFirstWin : BattleState()
    object TeamSecondWin : BattleState()
    object Draw : BattleState()
}
