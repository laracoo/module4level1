package com.example.project0418level1

import kotlin.random.Random

class Team(val countOfWarriors: Int) {
    val listWarriors: MutableList<Warrior> = mutableListOf()
    var healthOfTeam: Int = 0
    init {
        createTeam()
    }

    private fun createTeam() {
        for (item in 0..countOfWarriors) {
            val random = Random.nextInt(100)
            when {
                random < 20 -> {
                    listWarriors.add(JuniorWarrior())
                }
                random in 21..50 -> {
                    listWarriors.add(MasterWarrior())
                }
                random in 51..85 -> {
                    listWarriors.add(ProWarrior())
                }
                else -> {
                    listWarriors.add(BossWarrior())
                }
            }
        }
        var healthLevel: Int = 0
        for (i in 0 until listWarriors.size) {
            healthLevel += listWarriors[i].maxHealthLevel
        }
        healthOfTeam = healthLevel
    }
}
