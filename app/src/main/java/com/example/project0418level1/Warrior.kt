package com.example.project0418level1

interface Warrior {
    var isKilled: Boolean
    val chanceOfMissShot: Int
    val maxHealthLevel: Int
    val hitAccuracy: Int

    fun attack(warrior: Warrior)
    fun damageOfAttack(damage: Int)
}
